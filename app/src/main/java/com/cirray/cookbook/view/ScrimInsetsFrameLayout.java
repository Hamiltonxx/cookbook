package com.cirray.cookbook.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.cirray.cookbook.R;

/**
 * Created by Hamilton on 2016/5/24.
 */
public class ScrimInsetsFrameLayout extends FrameLayout{
    private Drawable mInsetForeground;
    private Rect mInsets;
    private Rect mTempRect = new Rect();
    private OnInsetsCallback mOnInsetsCallback;

    public ScrimInsetsFrameLayout(Context context) {
        this(context, null);
    }
    public ScrimInsetsFrameLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }
    public ScrimInsetsFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ScrimInsetsView, defStyleAttr, 0);
        if(a==null) return;
        mInsetForeground = a.getDrawable(R.styleable.ScrimInsetsView_scrimInsetForeground);
        a.recycle();
        setWillNotDraw(true);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        int width = getWidth();
        int height = getHeight();
        if(mInsets!=null && mInsetForeground!=null){
            int sc = canvas.save();
            canvas.translate(getScrollX(), getScrollY());

            //Top
            mTempRect.set(0,0, width, mInsets.top);
            mInsetForeground.setBounds(mTempRect);
            mInsetForeground.draw(canvas);

            //Bottom
            mTempRect.set(0, height-mInsets.bottom, width, height);
            mInsetForeground.setBounds(mTempRect);
            mInsetForeground.draw(canvas);

            //Left
            mTempRect.set(0, mInsets.top, mInsets.left, height-mInsets.bottom);
            mInsetForeground.setBounds(mTempRect);
            mInsetForeground.draw(canvas);

            //Right
            mTempRect.set(width-mInsets.right, mInsets.top, width, height);
            mInsetForeground.setBounds(mTempRect);
            mInsetForeground.draw(canvas);

            canvas.restoreToCount(sc);
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if(mInsetForeground!=null){
            mInsetForeground.setCallback(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if(mInsetForeground!=null){
            mInsetForeground.setCallback(null);
        }
    }

    public void setmOnInsetsCallback(OnInsetsCallback onInsetsCallback){
        mOnInsetsCallback = onInsetsCallback;
    }

    public static interface OnInsetsCallback{
        void onInsetsChanged(Rect insets);
    }
}
