package com.cirray.cookbook;

import android.app.Application;
import android.content.Context;

import com.cirray.cookbook.utility.ImageLoaderUtility;

/**
 * Created by Hamilton on 2016/5/7.
 */
public class CookbookApplication extends Application {
    private static CookbookApplication sInstance;

    public static Context getContext(){
        return sInstance;
    }
    public CookbookApplication(){
        sInstance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        try
        {
            Class.forName("android.os.AsyncTask");
        }
        catch(ClassNotFoundException e)
        {
            e.printStackTrace();
        }

        ImageLoaderUtility.init(getApplicationContext());
    }
}
