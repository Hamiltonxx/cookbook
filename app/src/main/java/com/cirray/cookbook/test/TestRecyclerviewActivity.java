package com.cirray.cookbook.test;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.cirray.cookbook.R;
import com.cirray.cookbook.test.adapter.ContactAdapter;
import com.cirray.cookbook.test.model.ContactInfo;

import java.util.ArrayList;
import java.util.List;

public class TestRecyclerviewActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_recyclerview);

        RecyclerView recList = (RecyclerView)findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        ContactAdapter ca = new ContactAdapter(createList(30));
        recList.setAdapter(ca);
    }

    private List<ContactInfo> createList(int n) {
        List<ContactInfo> list = new ArrayList<ContactInfo>();
        for(int i=0; i<n; i++){
            list.add(new ContactInfo("name"+i,"surname"+i,"hami"+i+"@163.com"));
        }
        return list;
    }
}
