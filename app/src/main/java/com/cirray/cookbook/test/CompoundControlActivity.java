package com.cirray.cookbook.test;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.cirray.cookbook.R;
import com.cirray.cookbook.test.compoundview.EntryFormView;

public class CompoundControlActivity extends Activity implements EntryFormView.OnEntrySubmittedListener{
    private ArrayAdapter<String> mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compound_control);

        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        ListView list = (ListView) findViewById(R.id.list);
        list.setAdapter(mAdapter);

        EntryFormView entryView = (EntryFormView) findViewById(R.id.entry_view);
        entryView.setListener(this);
    }

    @Override
    public void onEntrySubmitted(CharSequence name, CharSequence email) {
        mAdapter.add(name + ","+email);
        mAdapter.notifyDataSetChanged();
    }

}
