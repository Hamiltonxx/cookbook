package com.cirray.cookbook.test.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cirray.cookbook.R;

/**
 * Created by Hamilton on 2016/5/28.
 */
public class SeedRecyclerAdapter extends RecyclerView.Adapter<SeedRecyclerAdapter.SeedViewHolder>{
    String[] name = {"XS134-1", "XS134-2", "XS134-3", "XS134-4",
            "XS134-5", "XS134-6", "XS134-7", "XS134-8", "XS134-9", "XS134-10"};
    Context context;
    LayoutInflater inflater;

    public SeedRecyclerAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public SeedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.card_seed_list, parent, false);
        return new SeedViewHolder(v);
    }

    public void onBindViewHolder(SeedViewHolder holder, int position) {
        holder.tv1.setText(name[position]);
        holder.imageView.setTag(holder);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SeedViewHolder vHolder = (SeedViewHolder)v.getTag();
                int position = vHolder.getLayoutPosition();
                Toast.makeText(context, "This is position: "+position, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return name.length;
    }

    public class SeedViewHolder extends RecyclerView.ViewHolder{
        TextView tv1, tv2;
        ImageView imageView;
        public SeedViewHolder(View itemView) {
            super(itemView);

            tv1 = (TextView) itemView.findViewById(R.id.list_title);
            tv2 = (TextView) itemView.findViewById(R.id.list_desc);
            imageView = (ImageView) itemView.findViewById(R.id.list_avatar);
        }
    }
}
