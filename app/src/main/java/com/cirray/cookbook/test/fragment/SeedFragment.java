package com.cirray.cookbook.test.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cirray.cookbook.R;
import com.cirray.cookbook.test.adapter.SeedRecyclerAdapter;

public class SeedFragment extends Fragment {
    RecyclerView seedRecyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_seed, container, false);

        seedRecyclerView = (RecyclerView) rootView.findViewById(R.id.seed_list);
        SeedRecyclerAdapter adapter = new SeedRecyclerAdapter(getActivity());
        seedRecyclerView.setAdapter(adapter);
        seedRecyclerView.setHasFixedSize(true);

        seedRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return rootView;
    }

}
