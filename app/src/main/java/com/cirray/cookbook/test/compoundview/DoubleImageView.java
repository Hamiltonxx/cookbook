package com.cirray.cookbook.test.compoundview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

import com.cirray.cookbook.R;

/**
 * Created by Hamilton on 2016/5/10.
 */
public class DoubleImageView extends View {
    // Image Contents
    private Drawable leftDrawable, rightDrawable;
    // Text Contents
    private CharSequence text;
    private StaticLayout textLayout;
    // Text Drawing
    private TextPaint textPaint;
    private Point textOrigin;
    private int spacing;

    public DoubleImageView(Context context) {
        this(context, null);
    }

    public DoubleImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DoubleImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        textOrigin = new Point(0,0);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.DoubleImageView, 0, defStyleAttr);

        Drawable d = a.getDrawable(R.styleable.DoubleImageView_android_drawableLeft);
        if(d!=null){
            setLeftDrawable(d);
        }
        d = a.getDrawable(R.styleable.DoubleImageView_android_drawableRight);
        if(d!=null){
            setRightDrawable(d);
        }
        int spacing = a.getDimensionPixelSize(R.styleable.DoubleImageView_android_spacing, 0);
        setSpacing(spacing);
        int color = a.getColor(R.styleable.DoubleImageView_android_textColor, 0);
        textPaint.setColor(color);
        int rawSize = a.getDimensionPixelSize(R.styleable.DoubleImageView_android_textSize, 0);
        textPaint.setTextSize(rawSize);
        CharSequence text = a.getText(R.styleable.DoubleImageView_android_text);
        setText(text);

        a.recycle();
    }

    public void setLeftDrawable(Drawable left){
        leftDrawable = left;
        updateContentBounds();
        invalidate();
    }
    public void setRightDrawable(Drawable right){
        rightDrawable = right;
        updateContentBounds();
        invalidate();
    }
    public void setText(CharSequence text){
        if(!TextUtils.equals(this.text,text)){
            this.text = text;
            updateContentBounds();
            invalidate();
        }
    }
    public void setSpacing(int spacing){
        this.spacing = spacing;
        updateContentBounds();
        invalidate();
    }
    private  int getDesiredWidth(){
        int leftWidth, rightWidth, textWidth;
        if(leftDrawable == null){
            leftWidth = 0;
        }else{
            leftWidth = leftDrawable.getIntrinsicWidth();
        }
        if(rightDrawable == null){
            rightWidth = 0;
        }else{
            rightWidth = rightDrawable.getIntrinsicWidth();
        }
        if(textLayout == null){
            textWidth = 0;
        }else{
            textWidth = textLayout.getWidth();
        }
        return (int)(leftWidth*0.67f) + (int)(rightWidth*0.67f) + spacing + textWidth;
    }
    private  int getDesiredHeight(){
        int leftHeight, rightHeight;
        if(leftDrawable == null){
            leftHeight = 0;
        }else{
            leftHeight = leftDrawable.getIntrinsicHeight();
        }
        if(rightDrawable == null){
            rightHeight = 0;
        }else{
            rightHeight = rightDrawable.getIntrinsicHeight();
        }

        return (int)(leftHeight*0.67f) + (int)(rightHeight*0.67f);
    }
    private void updateContentBounds(){
        if(text == null){
            text = "";
        }
        float textWidth = textPaint.measureText(text, 0, text.length());
        textLayout = new StaticLayout(text, textPaint, (int)textWidth, Layout.Alignment.ALIGN_CENTER, 1f, 0f, true);
        int left = (getWidth() - getDesiredWidth()) / 2;
        int top = (getHeight() - getDesiredHeight()) / 2;

        if(leftDrawable != null){
            leftDrawable.setBounds(left, top, left+leftDrawable.getIntrinsicWidth(), top+leftDrawable.getIntrinsicHeight());
            left += (leftDrawable.getIntrinsicWidth()*0.33f);
            top += (leftDrawable.getIntrinsicHeight()*0.33f);
        }
        if(rightDrawable != null){
            rightDrawable.setBounds(left, top, left+rightDrawable.getIntrinsicWidth(), top+rightDrawable.getIntrinsicHeight());
            left = rightDrawable.getBounds().right + spacing;
        }
        if(textLayout != null){
            top = (getHeight() - textLayout.getHeight()) / 2;
            textOrigin.set(left, top);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //Get the width measurement
        int widthSize = View.resolveSize(getDesiredWidth(), widthMeasureSpec);
        //Get the height measurement
        int heightSize = View.resolveSize(getDesiredHeight(), heightMeasureSpec);

        //MUST call this to store the measurements
        setMeasuredDimension(widthSize, heightSize);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if(w!=oldw || h!=oldh){
            updateContentBounds();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(leftDrawable != null){
            leftDrawable.draw(canvas);
        }
        if(textLayout != null){
            canvas.save();
            canvas.translate(textOrigin.x, textOrigin.y);
            textLayout.draw(canvas);
            canvas.restore();
        }
        if(rightDrawable != null){
            rightDrawable.draw(canvas);
        }
    }
}
