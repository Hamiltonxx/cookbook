package com.cirray.cookbook.test.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cirray.cookbook.R;
import com.cirray.cookbook.test.FcbNavigationDrawerActivity;
import com.cirray.cookbook.test.fragment.ExperimentFragment;
import com.cirray.cookbook.test.fragment.SeedFragment;
import com.cirray.cookbook.test.fragment.ProfileFragment;


/**
 * Created by Hamilton on 2016/5/26.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{
    String[] titles;
    TypedArray icons;
    Context context;

    public RecyclerViewAdapter(String[] titles, TypedArray icons, Context context) {
        this.titles = titles;
        this.icons = icons;
        this.context = context;
    }

    /**
     *Its a inner class to RecyclerViewAdapter Class.
     *This ViewHolder class implements View.OnClickListener to handle click events.
     *If the itemType==1 ; it implies that the view is a single row_item with TextView and ImageView.
     *This ViewHolder describes an item view with respect to its place within the RecyclerView.
     *For every item there is a ViewHolder associated with it .
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView navTitle;
        ImageView navIcon;
        Context context;
        public ViewHolder(View itemView, int viewType, Context context) {
            super(itemView);
            this.context = context;
            itemView.setOnClickListener(this);
            if(viewType==1){
                navTitle = (TextView)itemView.findViewById(R.id.tv_navTitle);
                navIcon = (ImageView)itemView.findViewById(R.id.iv_navIcon);
            }
        }

        @Override
        public void onClick(View v) {
            FcbNavigationDrawerActivity activity = (FcbNavigationDrawerActivity)context;
            activity.getDrawerLayout().closeDrawers();
            FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();

            switch (getAdapterPosition()){
                case 1:
                    Fragment profileFragment = new ProfileFragment();
                    fragmentTransaction.replace(R.id.containerView, profileFragment);
                    fragmentTransaction.commit();
                    break;
                case 2:
                    Fragment experimentFragment = new ExperimentFragment();
                    fragmentTransaction.replace(R.id.containerView, experimentFragment);
                    fragmentTransaction.commit();
                    break;
                case 3:
                    Fragment seedFragment = new SeedFragment();
                    fragmentTransaction.replace(R.id.containerView, seedFragment);
                    fragmentTransaction.commit();
                    break;
            }
        }
    }

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if(viewType==1){
            View itemView = inflater.inflate(R.layout.fcb_drawer_item, null);
            return new ViewHolder(itemView, viewType, context);
        }else if(viewType==0){
            View headerView = inflater.inflate(R.layout.fcb_drawer_header, null);
            return new ViewHolder(headerView, viewType, context);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, int position) {
        if(position!=0){
            holder.navTitle.setText(titles[position-1]);
            holder.navIcon.setImageResource(icons.getResourceId(position-1, -1));
        }
    }

    @Override
    public int getItemCount() {
        return titles.length+1;
    }

    @Override
    public int getItemViewType(int position) {
        if(position==0) return 0;
        else return 1;
    }
}
