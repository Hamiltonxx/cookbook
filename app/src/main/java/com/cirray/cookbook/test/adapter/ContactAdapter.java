package com.cirray.cookbook.test.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cirray.cookbook.R;
import com.cirray.cookbook.test.model.ContactInfo;

import java.util.List;

/**
 * Created by Hamilton on 2016/5/8.
 */
public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {
    private List<ContactInfo>  contactList;

    public ContactAdapter(List<ContactInfo> contactList) {
        this.contactList = contactList;
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        ContactInfo ci = contactList.get(position);
        holder.vName.setText(ci.getName());
        holder.vSurname.setText(ci.getSurname());
        holder.vEmail.setText(ci.getEmail());
        holder.vTitle.setText(ci.getName()+" "+ci.getSurname());
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout_test, parent, false);
        return new ContactViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder{
        protected TextView vName;
        protected TextView vSurname;
        protected TextView vEmail;
        protected TextView vTitle;
        public ContactViewHolder(View itemView) {
            super(itemView);
            vName = (TextView)itemView.findViewById(R.id.txtName);
            vSurname = (TextView)itemView.findViewById(R.id.txtSurname);
            vEmail = (TextView)itemView.findViewById(R.id.txtEmail);
            vTitle = (TextView)itemView.findViewById(R.id.txtTitle);
        }
    }
}
