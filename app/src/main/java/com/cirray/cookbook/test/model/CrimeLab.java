package com.cirray.cookbook.test.model;

import android.content.Context;
import android.util.Log;

import com.cirray.cookbook.test.tool.CriminalIntentJSONSerializer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Hamilton on 2016/5/29.
 */
public class CrimeLab {
    private static final String TAG = "CrimeLab";
    private static final String FILENAME = "crimes.json";

    private static CrimeLab sCrimeLab;
    private ArrayList<Crime> crimes;
    private CriminalIntentJSONSerializer serializer;

    private Context context;

    private CrimeLab(Context context){
        this.context = context;
        serializer = new CriminalIntentJSONSerializer(context, FILENAME);
//        crimes = new ArrayList<>();
//        for(int i=0; i<100; i++){
//            Crime crime = new Crime();
//            crime.setTitle("Crime #"+i);
//            crime.setSolved(i%2 == 0);
//            crimes.add(crime);
//        }
        try{
            crimes = serializer.loadCrimes();
        }catch (Exception e){
            crimes = new ArrayList<Crime>();
            Log.e(TAG, "Error loading crimes: ", e);
        }
    }

    public static CrimeLab get(Context context){
        if(sCrimeLab == null){
            sCrimeLab = new CrimeLab(context);
        }
        return sCrimeLab;
    }

    public Crime getCrime(UUID id){
        for(Crime crime : crimes){
            if(crime.getId().equals(id)){
                return crime;
            }
        }
        return null;
    }

    public void addCrime(Crime crime){
        crimes.add(crime);
        saveCrimes();
    }

    public List<Crime> getCrimes(){
        return crimes;
    }

    public void deleteCrime(Crime crime){
        crimes.remove(crime);
        saveCrimes();
    }

    private boolean saveCrimes() {
        try{
            serializer.saveCrimes(crimes);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

}
