package com.cirray.cookbook.test.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.cirray.cookbook.R;
import com.cirray.cookbook.test.fragment.CrimeListFragment;

public class CrimeListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_fragment);

        FragmentManager fm = getSupportFragmentManager();
        CrimeListFragment crimeListFragment = new CrimeListFragment();
        fm.beginTransaction().add(R.id.fragment_container, crimeListFragment).commit();
    }
}
