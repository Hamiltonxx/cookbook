package com.cirray.cookbook.test;

import android.app.Activity;
import android.os.Bundle;

import com.cirray.cookbook.R;

public class AspectImageActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aspect_image);
    }
}
