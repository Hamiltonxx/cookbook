package com.cirray.cookbook.test.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.cirray.cookbook.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonTutorialActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json_tutorial);

        TextView output = (TextView)findViewById(R.id.tv1);
//        String strJson = "
//        {
//            'Employee':[
//            {
//                'id':'01',
//                'name':'Gopal Varma',
//                'salary':'5000'
//            },{
//                'id':'02',
//                'name':'Saikramk',
//                'salary':'40000'
//            },{
//                'id':'03',
//                'name':'Hamilton',
//                'salary':'60000'
//            }]
//        }";
        String strJson = "{'employee':[{'id':'01','name':'Gopal Varma','salary':'50000'},{'id':'02','name':'Yamasaki','salary':'40000'},{'id':'03','name':'Hamilton','salary':'60000'}]}";
        String data = "";
        try{
            JSONObject jsonRootObject = new JSONObject(strJson);
            JSONArray jsonArray = jsonRootObject.getJSONArray("employee");
            for(int i=0; i<jsonArray.length(); i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = Integer.parseInt(jsonObject.getString("id"));
                Log.i("TAG id",id+"");
                String name = jsonObject.getString("name");
                Log.i("TAG name",name);
                float salary = Float.parseFloat(jsonObject.getString("salary"));
                data += "Node"+i+":\n id= "+id+"\n Name="+name+"\n Salary="+salary+"\n";
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
        output.setText(data);
    }
}
