package com.cirray.cookbook.test.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.cirray.cookbook.R;
import com.cirray.cookbook.test.activity.CrimeActivity;
import com.cirray.cookbook.test.model.Crime;

import java.util.List;

/**
 * Created by Hamilton on 2016/5/29.
 */
public class CrimeAdapter extends RecyclerView.Adapter<CrimeAdapter.CrimeHolder>{
    private Context context;
    private List<Crime> crimes;
    private TextView titleTextView;
    private TextView dateTextView;
    private CheckBox solvedCheckBox;

    public CrimeAdapter(Context context, List<Crime> crimes) {
        super();
        this.context = context;
        this.crimes = crimes;
    }

    @Override
    public CrimeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.card_crime, parent, false);
        return new CrimeHolder(view);
    }

    @Override
    public void onBindViewHolder(CrimeHolder holder, int position) {
        Crime crime = crimes.get(position);
        titleTextView.setText(crime.getTitle());
        dateTextView.setText(crime.getDate().toString());
        solvedCheckBox.setChecked(crime.isSolved());
    }

    @Override
    public int getItemCount() {
        return crimes.size();
    }

    class CrimeHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public CrimeHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            titleTextView = (TextView)itemView.findViewById(R.id.card_crime_title);
            dateTextView = (TextView)itemView.findViewById(R.id.card_crime_date);
            solvedCheckBox = (CheckBox) itemView.findViewById(R.id.card_crime_solved);
        }

        @Override
        public void onClick(View v) {
            Crime crime = crimes.get(getAdapterPosition());
            Toast.makeText(context, crime.getTitle()+" clicked", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(context, CrimeActivity.class);
            intent.putExtra("crimeObj", crime);
            context.startActivity(intent);
        }
    }
}
