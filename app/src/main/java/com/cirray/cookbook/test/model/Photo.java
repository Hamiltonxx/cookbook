package com.cirray.cookbook.test.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Hamilton on 2016/5/30.
 */
public class Photo implements Serializable {
    private static final long serialVersionUID = 1L;

    private static final String JSON_FILENAME = "filename";
    private String fileName;

    public Photo() {
        this(UUID.randomUUID().toString()+".jpg");
    }
    public Photo(String fileName) {
        this.fileName = fileName;
    }
    public Photo(JSONObject json) throws JSONException{
        fileName = json.getString(JSON_FILENAME);
    }

    public JSONObject toJSON() throws JSONException{
        JSONObject json = new JSONObject();
        json.put(JSON_FILENAME, fileName);
        return json;
    }

    public String getFileName(){
        return fileName;
    }
}
