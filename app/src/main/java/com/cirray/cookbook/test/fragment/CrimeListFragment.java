package com.cirray.cookbook.test.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.android.multiselector.ModalMultiSelectorCallback;
import com.bignerdranch.android.multiselector.MultiSelector;
import com.cirray.cookbook.R;
import com.cirray.cookbook.test.adapter.CrimeAdapter;
import com.cirray.cookbook.test.model.Crime;
import com.cirray.cookbook.test.model.CrimeLab;

import java.util.ArrayList;
import java.util.List;

public class CrimeListFragment extends Fragment {
    private RecyclerView crimeRecyclerView;
    private CrimeAdapter adapter;

    private MultiSelector multiSelector = new MultiSelector();
    private ArrayList<Crime> crimes;
    private boolean subtitleVisible;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle("Crimes");
        setRetainInstance(true);
        subtitleVisible = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_crime_list, container, false);
        if(subtitleVisible){
            ((AppCompatActivity)getActivity()).getSupportActionBar().setSubtitle("Sometimes tolerance is not a virtue.");
        }
        crimeRecyclerView = (RecyclerView)rootView.findViewById(R.id.crime_recycler_view);
        crimeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        CrimeLab crimeLab = CrimeLab.get(getActivity());
        List<Crime> crimes = crimeLab.getCrimes();
        adapter = new CrimeAdapter(getActivity(),crimes);
        crimeRecyclerView.setAdapter(adapter);
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        crimeRecyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_crime_list, menu);
        MenuItem showSubtitle = menu.findItem(R.id.show_subtitle);
        if(subtitleVisible && showSubtitle!=null){
            showSubtitle.setTitle("Hide Subtitle");
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getActivity().getMenuInflater().inflate(R.menu.crime_list_item_context, menu);
    }

    private ActionMode.Callback deleteMode = new ModalMultiSelectorCallback(multiSelector) {
        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            getActivity().getMenuInflater().inflate(R.menu.crime_list_item_context, menu);
            return true;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()){
                case R.id.delete_crime:
                    mode.finish();
                    for(int i=crimes.size()-1; i>=0; i--){
                        if(multiSelector.isSelected(i,0)){
                            Crime crime = crimes.get(i);
                            CrimeLab.get(getActivity()).deleteCrime(crime);
                            crimeRecyclerView.getAdapter().notifyDataSetChanged();
                        }
                    }
                    multiSelector.clearSelections();
                    return true;
                default:
                    break;
            }
            return false;
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.new_crime:
                final Crime crime = new Crime();
                CrimeLab.get(getActivity()).addCrime(crime);
                crimeRecyclerView.getAdapter().notifyDataSetChanged();
                return true;
            case R.id.show_subtitle:
                ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
                if(actionBar.getSubtitle()==null){
                    actionBar.setSubtitle("Hami Set Subtitle");
                    subtitleVisible = true;
                    item.setTitle("Hide Subtitle");
                }else{
                    actionBar.setSubtitle(null);
                    subtitleVisible = false;
                    item.setTitle("Show Subtitle");
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
