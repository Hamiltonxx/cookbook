package com.cirray.cookbook.test;

import android.app.Activity;
import android.os.Bundle;

import com.cirray.cookbook.R;

public class TestCustomViewActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_custom_view);
    }
}
