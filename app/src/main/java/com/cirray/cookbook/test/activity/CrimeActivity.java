package com.cirray.cookbook.test.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.cirray.cookbook.R;
import com.cirray.cookbook.test.fragment.CrimeFragment;
import com.cirray.cookbook.test.model.Crime;

public class CrimeActivity extends AppCompatActivity {
    private Crime crime;
//    @Override
//    protected Fragment createFragment() {
//        return new CrimeFragment();
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_fragment);

        Intent intent = getIntent();
        crime = (Crime)intent.getSerializableExtra("crimeObj");
        Log.i("CrimeActivity TAG",crime.getTitle());

        FragmentManager fm = getSupportFragmentManager();
        CrimeFragment crimeFragment = new CrimeFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("crimeObj",crime);
        crimeFragment.setArguments(bundle);
        fm.beginTransaction().add(R.id.fragment_container,crimeFragment).commit();
    }
}
