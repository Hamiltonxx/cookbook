package com.cirray.cookbook;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.cirray.cookbook.adapter.DrawerAdapter;
import com.cirray.cookbook.database.model.CategoryModel;
import com.cirray.cookbook.view.ScrimInsetsFrameLayout;

import java.util.List;

public class MainActivity extends AppCompatActivity implements DrawerAdapter.CategoryViewHolder.OnItemClickListener{
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ScrimInsetsFrameLayout mDrawerScrimInsetsFrameLayout;
    private DrawerAdapter mDrawerAdapter;
    private CharSequence mTitle;
    private CharSequence mDrawerTitle;
    private List<CategoryModel> mCategoryList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onItemClick(View view, int position, long id, int viewType) {

    }
}
