package com.cirray.cookbook.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.android.multiselector.MultiSelector;
import com.bignerdranch.android.multiselector.SingleSelector;
import com.bignerdranch.android.multiselector.SwappingHolder;
import com.cirray.cookbook.CookbookApplication;
import com.cirray.cookbook.R;
import com.cirray.cookbook.database.model.CategoryModel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.List;

/**
 * Created by Hamilton on 2016/5/7.
 */
public class DrawerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_CATEGORY = 1;

    private List<CategoryModel> mCategoryList;
    private CategoryViewHolder.OnItemClickListener mListener;
    private SingleSelector mSingleSelector = new SingleSelector();
    private ImageLoader mImageLoader = ImageLoader.getInstance();
    private DisplayImageOptions mDisplayImageOptions;
    private ImageLoadingListener mImageLoadingListener;

    public DrawerAdapter(List<CategoryModel> categoryList, CategoryViewHolder.OnItemClickListener listener) {
        mCategoryList = categoryList;
        mListener = listener;
        mSingleSelector.setSelectable(true);
//        setupImageLoader();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        // inflate view and create view holder
        if(viewType==VIEW_TYPE_HEADER){
//            View view = inflater.inflate(R.layout.)
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public static final class CategoryViewHolder extends SwappingHolder implements View.OnClickListener{
        private TextView mNameTextView;
        private TextView mCountTextView;
        private ImageView mIconImageView;
        private OnItemClickListener mListener;
        private SingleSelector mSingleSelector;
        private ImageLoader mImageLoader;
        private DisplayImageOptions mDisplayImageOptions;
        private ImageLoadingListener mImageLoadingListener;

        public interface OnItemClickListener{
            void onItemClick(View view, int position, long id, int viewType);
        }
        public CategoryViewHolder(View itemView, OnItemClickListener listener, SingleSelector singleSelector, ImageLoader imageLoader, DisplayImageOptions displayImageOptions, ImageLoadingListener imageLoadingListener) {
            super(itemView, singleSelector);
            mListener = listener;
            mSingleSelector = singleSelector;
            mImageLoader = imageLoader;
            mDisplayImageOptions = displayImageOptions;
            mImageLoadingListener = imageLoadingListener;

            // set selection background
            setSelectionModeBackgroundDrawable(ContextCompat.getDrawable(CookbookApplication.getContext(), R.drawable.selector_selectable_item_bg));
            setSelectionModeStateListAnimator(null);

            // set listener
            itemView.setOnClickListener(this);

            //find views
            mNameTextView = (TextView) itemView.findViewById(R.id.drawer_item_name);
            mCountTextView = (TextView) itemView.findViewById(R.id.drawer_item_count);
            mIconImageView = (ImageView) itemView.findViewById(R.id.drawer_item_icon);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if(position!=RecyclerView.NO_POSITION){
                mListener.onItemClick(v, position, getItemId(), getItemViewType());
            }
        }

        public void bindData(CategoryModel category){
            mNameTextView.setText(category.getName());
            mCountTextView.setVisibility(View.GONE);
            mImageLoader.displayImage(category.getImage(), mIconImageView, mDisplayImageOptions, mImageLoadingListener);
        }
    }
}
